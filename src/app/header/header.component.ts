import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Output() featureSelected = new EventEmitter<string>();
  onSelect(feature: string) {
    this.featureSelected.emit(feature);
  }
}
/**
 * to make it listanabled from parent component @output
 * @input decorator binds a property within one component (child component) to receive
 * a value from another component(parent component)
 * @output decorator binds a property of a component to send data from one component
 * (child component) to calling component (parent component)
 */
