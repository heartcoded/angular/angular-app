import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Recipe} from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  @Output() recipeWasSelected = new EventEmitter<Recipe>();
  recipes: Recipe[] = [
    new Recipe('A test recipe', 'This is a simple recipe','https://static.thenounproject.com/png/82540-200.png' ),
    new Recipe('Second recipe','Description for second recipe','https://png.pngtree.com/svg/20160202/7af036a39c.png')
  ];
  constructor() {
    for(const recipe of this.recipes) {
      console.log(recipe.name);
    }
  }

  ngOnInit() {
  }

  onRecipeSelected(recipe: Recipe) {
    this.recipeWasSelected.emit(recipe);
  }

}
